#include "World.h"


World::World(void)
{
}


World::~World(void)
{
}

void World::registerGameObjectConstructor( constructor_key& key, IGameObjectConstructor* constructor )
{
	if(mConstructorMap.find(key) != mConstructorMap.end())
	{
		throw new std::exception("Cannot register GameObjectConstructor: Key is already in use!");
	}

	mConstructorMap[key] = constructor;
}
