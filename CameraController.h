#ifndef _CameraController_h_
#define _CameraController_h_

#include <OgreRoot.h>
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>
#include <OgreCamera.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

#include "Updatable.h"

class CameraController : public OIS::KeyListener, public OIS::MouseListener, public Updatable
{
public:
	static const Ogre::Radian	ROTATION_LIMIT;
	static const Ogre::Real		SPEED_LIMIT;
	static const Ogre::Real		ACCELERATION;
	static const float			INTERPOLATION_STEP;

	CameraController( Ogre::Camera* camera );
	virtual ~CameraController(void);

	void update( Ogre::Real time );

	/*
	 * Key listeners
	 */
    bool keyPressed( const OIS::KeyEvent &e );
    bool keyReleased( const OIS::KeyEvent &e );

	/*
	 *  mouse listeners
	 */
    bool mouseMoved( const OIS::MouseEvent &e );
    bool mousePressed( const OIS::MouseEvent &e, OIS::MouseButtonID id );
    bool mouseReleased( const OIS::MouseEvent &e, OIS::MouseButtonID id );

	virtual void setTargetDirection( const Ogre::Vector3& direction );
	virtual const Ogre::Vector3& CameraController::getRelativeGroundDirection( const Ogre::Vector3& direction );
	virtual void CameraController::setKeyboardPanDirection( void );

	bool doPan( void ){ return mDoMousePan || mDoPanDown || mDoPanLeft || mDoPanUp || mDoPanRight; }
	bool doKeyboardPan( void ){ return mDoPanDown || mDoPanLeft || mDoPanUp || mDoPanRight; }
private:
	Ogre::Camera*		mCamera;
	Ogre::Plane			mGroundPlane;
	Ogre::Vector3		mLastMousePosition;
	Ogre::SceneNode*	mTargetNode;
	Ogre::SceneNode*	mCameraNode;
	Ogre::Vector3		mDirection;
	Ogre::Vector3		mTargetDirection;
	float				mAlpha;
	float				mSpeed;
	bool				mDoPanLeft;
	bool				mDoPanRight;
	bool				mDoPanUp;
	bool				mDoPanDown;
	bool				mDoMousePan;
	bool				mDoRotate;

	void panAccelerate( Ogre::Real acceleration );
	void panDecay( void );
};

#endif