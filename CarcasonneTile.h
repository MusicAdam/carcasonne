#ifndef _CarcasonneTile_h_
#define _CarcasonneTile_h_

#include <cstdint>

#include "TileObject.h"
#include <iostream>

typedef uint8_t tile_flags;

#define TILE_FLAG_BITS 8

class CarcasonneTile : public TileObject
{
public:
	//TODO: Need to solve the rotation problem...
	static const tile_flags MASK_SIDE_LEFT		=	0x1;
	static const tile_flags MASK_SIDE_BOTTOM	=	0X2;
	static const tile_flags MASK_SIDE_RIGHT		=	0X4;
	static const tile_flags MASK_SIDE_TOP		=	0X8;
	static const tile_flags MASK_LEFT_HIGH		=	0x10;
	static const tile_flags MASK_BOTTOM_HIGH	=	0x20;
	static const tile_flags MASK_RIGHT_HIGH		=	0x40;
	static const tile_flags MASK_TOP_HIGH		=	0x80;
	static const tile_flags MASK_LOW_NIBBLE		=   0x0F;
	static const tile_flags MASK_HIGH_NIBBLE	=   0xF0;

	CarcasonneTile( gameObject_key key );
	virtual ~CarcasonneTile(void);

	void rotateLeft( void );
	void rotateRight( void );

	void debug_output();

	virtual void onHighlight(); //TODO: delete
private:
	//Defines a tile with no rotations
	struct tile_definition_t {
		tile_flags road_flags; //High bits are sides on which roads end, low bits are sides which have roads
		tile_flags city_flags; //High bits are set if city has a shield, low bits are set for city connections
		tile_flags field_flags; //High bits are set if the field has a cloister, low bits are set for field connections
		tile_flags misc_flags; //Contains flags for shield and cloister
		//(road_flags & city_flags) | (field_flags & city_flags) should == 0 because there should because a tile cannot connect on the same side as a city

		tile_definition_t() 
			: road_flags(0), 
			city_flags(0), 
			field_flags(0), 
			misc_flags(0)
		{}

		void rotateLeft() {
			if(hasRoadTop()){
				road_flags = (road_flags & MASK_HIGH_NIBBLE) | ((_rotl8(getRoadConnectionFlags(), 1) | MASK_SIDE_LEFT) & MASK_LOW_NIBBLE);
			}else{
				road_flags = (road_flags & MASK_HIGH_NIBBLE) | _rotl8(getRoadConnectionFlags(), 1);
			}
		}

		void rotateRight(){
			if(hasRoadLeft()){
				road_flags = (road_flags & MASK_HIGH_NIBBLE) | ((_rotl8(getRoadConnectionFlags(), -1) | MASK_SIDE_TOP) & MASK_LOW_NIBBLE);
			}else{
				road_flags = (road_flags & MASK_HIGH_NIBBLE) | _rotl8(getRoadConnectionFlags(), -1);
			}
		}

		tile_flags getRoadConnectionFlags(){
			return road_flags & MASK_LOW_NIBBLE;
		}

		bool hasRoadTop(){
			return (road_flags & MASK_SIDE_TOP) > 0;
		}

		bool hasRoadLeft(){
			return (road_flags & MASK_SIDE_LEFT) > 0;
		}

	} tile_definition;
};

#endif
