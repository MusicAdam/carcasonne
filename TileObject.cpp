#include "TileObject.h"
#include "World.h"
#include "GameBoard.h"

#include <iostream>

REGISTER_GAME_OBJECT( TileObject );

const float TileObject::UNIT_SIZE = 100;
const gameObject_key TileObject::INDEX_SEPARATOR = ",";


index_pair TileObject::KeyToIndexPair( const gameObject_key& key )
{
	Ogre::StringVector keySplit = Ogre::StringUtil::split( key, INDEX_SEPARATOR );
	if( keySplit.size() == 2 )
	{
		tile_index x, y;
		x = Ogre::StringConverter::parseInt( keySplit[0] );
		y = Ogre::StringConverter::parseInt( keySplit[1] );
		return std::pair< tile_index, tile_index >( x, y ); 
	}else{
		return std::pair< tile_index, tile_index >(0, 0);
	}
}
gameObject_key TileObject::IndexPairToKey( const std::pair< tile_index, tile_index >& indexPair )
{
	return TileObject::IndexPairToKey(indexPair.first, indexPair.second);

}
gameObject_key TileObject::IndexPairToKey( tile_index x, tile_index y )
{
	std::string xStr, yStr;
	xStr = std::to_string( x );
	yStr = std::to_string( y );
	return xStr + INDEX_SEPARATOR + yStr;
}

gameObject_key TileObject::OgreNameToKey( std::string name )
{
	Ogre::StringVector nameSplit = Ogre::StringUtil::split( name, NAME_SEPARATOR );
	if( nameSplit.size() == 2 )
	{
		return nameSplit[0];
	}else{
		return "";
	}
}

std::string TileObject::IndexPairToSceneNodeName( tile_index x, tile_index y )
{
	std::string key = IndexPairToKey( x, y );
	return key + NAME_SEPARATOR + SCENE_NODE_NAME_TAG;
}

TileObject::TileObject( gameObject_key key )
	:	GameObject(key),
		mHighlighted(false)
{
	 index_pair indexPair = KeyToIndexPair( mKey );
	 mIndexX = indexPair.first;
	 mIndexY = indexPair.second;
}


TileObject::~TileObject(void)
{
	/*if(mEntity != NULL && mNode != NULL)
	{
		mEntity->detachFromParent();
		sceneMgr->destroyEntity(mEntity);
		mEntity = NULL;

		mNode->detachAllObjects();
		sceneMgr->destroySceneNode(mNode);
		mNode = NULL;
	}*/
}

Ogre::SceneNode* TileObject::spawn( Ogre::SceneNode* parentNode )
{
	GameObject::spawn();
	
	Ogre::Entity* ent	= Ogre::Root::getSingleton().getSceneManager("SceneManager")->createEntity(getKey() + NAME_SEPARATOR + ENTITY_NAME_TAG, Ogre::SceneManager::PT_CUBE);
	ent->setUserAny( Ogre::Any( this ) );
	mSceneNode->attachObject(ent);
	mSceneNode->setScale(1, .1, 1); //TODO
	mSceneNode->setPosition(mIndexX * UNIT_SIZE, 0, mIndexY * UNIT_SIZE);
	//mSceneNode->showBoundingBox(true);

	GameBoard* board  = dynamic_cast<GameBoard*>(Ogre::any_cast<GameObject*>(parentNode->getUserAny()) );
	if( parentNode && board != 0 ){
		board->addTile( this );
	}else if(parentNode){
		parentNode->addChild(mSceneNode);
	}

	std::cout << "spawned " << mSceneNode->getName() << " at " << std::to_string(mSceneNode->getPosition().x) << ", " << std::to_string(mSceneNode->getPosition().z) << std::endl;

	return mSceneNode;
}

void TileObject::update( Ogre::Real time )
{
}

void TileObject::onHighlight()
{
	if(mHighlighted) return;

	mHighlighted = true;
	getSceneNode()->setScale(1, .2, 1);
}

void TileObject::onUnhighlight()
{
	if(!mHighlighted) return;

	mHighlighted = false;
	getSceneNode()->setScale(1, .1, 1);
}

bool TileObject::isHighlighted()
{
	return mHighlighted;
}


bool 	TileObject::mouseMoved (const OIS::MouseEvent& arg)
{
	return true;
}
bool 	TileObject::mousePressed (const OIS::MouseEvent& arg, OIS::MouseButtonID id)
{
	return true;
}
bool 	TileObject::mouseReleased (const OIS::MouseEvent& arg, OIS::MouseButtonID id)
{
	return true;
}