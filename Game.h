/*
-----------------------------------------------------------------------------
Filename:    Game.h
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#ifndef __Game_h_
#define __Game_h_

#include "BaseApplication.h"
#include "GameBoard.h"

//---------------------------------------------------------------------------

class Game : public BaseApplication
{
public:
    Game(void);
    virtual ~Game(void);
protected:
    virtual void createScene(void);
private: 
	Ogre::RaySceneQuery*	mRayScnQuery;
	GameBoard*				mGameBoard;
};

//---------------------------------------------------------------------------

#endif // #ifndef __Game_h_

//---------------------------------------------------------------------------
