#ifndef _GameBoard_h_
#define _GameBoard_h_

#include <vector>
#include <iostream>
#include <stdexcept>

#include <OgreSceneNode.h>
#include <OgreRoot.h>
#include <OgreSceneManager.h>
#include <OISEvents.h>
#include <OISMouse.h>

#include "World.h"
#include "TileObject.h"
#include "Updatable.h"

//
//GameBoard manages TileObjects
class GameBoard : public GameObject, public OIS::MouseListener
{
public:
	static const tile_index SIZE = 9;

	GameBoard( gameObject_key key );
	virtual ~GameBoard( void );
	
	virtual void update( Ogre::Real time );
	//
	//Gets a pointer to a the TileObject at x, y or NULL 
	//if the tile doesn't exist
	virtual TileObject* getTile( tile_index x, tile_index y );

	/*
	 * Adds a tile to the board
	 * tile is added by inserting TileSceneNode into our scenenode
	 * triggering scenenode events
	 */
	virtual void addTile( TileObject* tile );

	/*
	 * Removes a tile by removing it from our SceneNode
	 */
	virtual void removeTile( TileObject* tile );
	virtual void removeTile( gameObject_key tileKey );

	/*
	 *	Gets the width of the board in tiles
	 */
	virtual Ogre::Real getWidth( void ) const;

	/*
	 *	Gets the height of the board in tiles
	 */
	virtual Ogre::Real getHeight( void ) const;

	virtual TileObject* getHighlightedTile(){ return mHighlightedTile; }

	/*
	 * OIS Mouse Events
	 */
	virtual bool 	mouseMoved		(const OIS::MouseEvent& arg);
	virtual bool 	mousePressed	(const OIS::MouseEvent& arg, OIS::MouseButtonID id);
	virtual bool 	mouseReleased	(const OIS::MouseEvent& arg, OIS::MouseButtonID id);

	/*
	 * Gets the tile at the intersection of the board and the ray
	 */ 
	virtual TileObject* getRayIntersection( Ogre::Ray ray );

	void debug_fillBoard( void );
protected:
	bool mValid;

	virtual void validate( void );
private:
	TileObject*				mHighlightedTile;	//The tile underneath the mouse, or 0 
	Ogre::AxisAlignedBox	mBoundingBox;			//Bounds
};

#endif