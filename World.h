#ifndef _World_h_
#define _World_h_

#include "GameObject.h"

//
//World manages GameObjects and the Ogre SceneManager
class World
{
public:
	typedef std::unordered_map<gameObject_key, GameObject*> object_map;
	typedef std::pair<gameObject_key, GameObject*> object_pair;
	typedef const std::string constructor_key;

	World( void );
	World(World const&);
	~World( void );

	void operator=(World const&);

	template<typename T>
	T* getGameObject( gameObject_key key ) 
	{
		return static_cast<T*>(mMap[key]);
	}

	template<typename T>
	T* createGameObject( constructor_key& key, gameObject_key obj_key = "" )
	{
		auto ctorPair = mConstructorMap.find( key );
		if( ctorPair == mConstructorMap.end() )
		{
			throw new std::exception("Cannot construct game object: Unregistered object type!");
		}

		if(obj_key == ""){
			obj_key = std::to_string(mNextId);
			mNextId++;
		}

		GameObject* obj = ctorPair->second->create( obj_key );
		T* t_obj = static_cast< T* >( obj );
		return t_obj;
	}

	void registerGameObjectConstructor( constructor_key& key, IGameObjectConstructor* constructor );

	static World& World::getInstance()
	{
		static World inst;
		return inst;
	}
protected:
	object_map		mMap;
	unsigned int	mNextId;


private:	
	typedef std::map<constructor_key, IGameObjectConstructor*> constructor_map;
	
	constructor_map mConstructorMap;
};


template<typename T>
class GameObjectConstructor : public IGameObjectConstructor
{
public:
	GameObjectConstructor( const std::string& key )
	{
		World::getInstance().registerGameObjectConstructor( key, this );
	}

	virtual T* create( gameObject_key key ) const
	{
		T* obj = new T( key  );
		return obj;
	}
};

#define REGISTER_GAME_OBJECT(T) static GameObjectConstructor< T > ctor( #T );

#endif