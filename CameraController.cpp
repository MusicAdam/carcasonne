#include "CameraController.h"
#include <iostream>
#include "DebugDrawer.h"

const Ogre::Radian	CameraController::ROTATION_LIMIT		= Ogre::Radian(.03727);
const Ogre::Real	CameraController::SPEED_LIMIT			= 15;
const Ogre::Real	CameraController::ACCELERATION			= .55f;
const Ogre::Real	CameraController::INTERPOLATION_STEP	= .1f; //Should divide into 1 evenly

//TODO: Rotation and velocity
CameraController::CameraController( Ogre::Camera* camera )
	:	mCamera( camera ),
		mDoPanLeft( false ),
		mDoPanRight( false ),
		mDoPanUp( false ),
		mDoPanDown( false ),
		mDoMousePan( false ),
		mDoRotate( false ),
		mDirection(Ogre::Vector3(1, 0, 0)),
		mTargetDirection(),
		mAlpha(0),
		mSpeed(0),
		mGroundPlane( Ogre::Vector3( 0, 1, 0 ), 1 )
{
	auto sceneMgr = Ogre::Root::getSingleton().getSceneManager("SceneManager");
	mTargetNode = sceneMgr->createSceneNode("cameraController_target");
	mCameraNode = sceneMgr->createSceneNode("cameraController_node");
	mCameraNode->setPosition(0, 1000, 1000);

	mCameraNode->attachObject(mCamera);
	mTargetNode->addChild(mCameraNode);
	sceneMgr->getRootSceneNode()->addChild(mTargetNode);
	mCamera->lookAt(mTargetNode->getPosition());
}


CameraController::~CameraController(void)
{
}

void CameraController::update( Ogre::Real time )
{
	if(doPan()){
		panAccelerate( ACCELERATION );
	}else{
		panDecay();
	}
	Ogre::Vector3 realDir;

	if(mDirection != mTargetDirection){
		mAlpha += INTERPOLATION_STEP;
		realDir = mDirection + (mTargetDirection - mDirection) * mAlpha;
		realDir.normalise();

		if(mAlpha >= 1){
			mDirection = mTargetDirection;
			mAlpha = 0;
		}
		//std::cout << realDir << ", " << mAlpha << std::endl;
	}else{
		realDir = mDirection;
	}

	if(mSpeed > 0){
		mTargetNode->translate( realDir * mSpeed );
	}
	/*
	Ogre::Vector3 start = mTargetNode->_getDerivedPosition() + Ogre::Vector3(0, 10, 0);
	//std::cout << mCameraNode->_getDerivedPosition().x << ", " << mCameraNode->_getDerivedPosition().y << ", " << mCameraNode->_getDerivedPosition().z << std::endl;
	Ogre::Vector3 end = start + mTargetDirection * 1000;
	DebugDrawer::getSingleton().drawLine(start, end, Ogre::ColourValue(1.0f, 0, 0, 1.0f));
	end = start + mDirection * 1000;
	//DebugDrawer::getSingleton().drawLine(start, end, Ogre::ColourValue(1.0f, 1.0f, 0, 1.0f));
	end = start + realDir * 1000;
	DebugDrawer::getSingleton().drawLine(start, end, Ogre::ColourValue(1.0f, 1.0f, 0, 1.0f));
	mCamera->lookAt(mTargetNode->getPosition());
	*/
}

bool CameraController::keyPressed( const OIS::KeyEvent &e )
{
	if(mDoMousePan) return true;

	//TODO: Move these to an input mapper
	if(e.key == OIS::KeyCode::KC_A){
		mDoPanLeft = true;
	}else if(e.key == OIS::KeyCode::KC_W){
		mDoPanUp = true;
	}else if(e.key == OIS::KeyCode::KC_S){
		mDoPanDown = true;
	}else if(e.key == OIS::KeyCode::KC_D){
		mDoPanRight = true;
	}

	setKeyboardPanDirection();

	return true;
}
bool CameraController::keyReleased( const OIS::KeyEvent &e )
{
	if(e.key == OIS::KeyCode::KC_A)
		mDoPanLeft = false;
	if(e.key == OIS::KeyCode::KC_W)
		mDoPanUp = false;
	if(e.key == OIS::KeyCode::KC_S)
		mDoPanDown = false;
	if(e.key == OIS::KeyCode::KC_D)
		mDoPanRight = false;

	if(!mDoMousePan)
		setKeyboardPanDirection();

	return true;
}


bool CameraController::mouseMoved( const OIS::MouseEvent &e )
{
	if(mDoMousePan && !doKeyboardPan()){
		Ogre::Vector3 delta = Ogre::Vector3(e.state.X.rel, 0, e.state.Y.rel);
		delta.normalise();
		setTargetDirection( mTargetNode->getOrientation() * delta );
	}

	if(mDoRotate){
		Ogre::Radian rot = Ogre::Radian(e.state.X.rel / Ogre::Math::PI);

		if(rot > Ogre::Radian(ROTATION_LIMIT)){
			rot = ROTATION_LIMIT;
		}else if(rot < -1 * ROTATION_LIMIT){
			rot = -1 * ROTATION_LIMIT;
		}

		mTargetNode->rotate(Ogre::Vector3::UNIT_Y, rot);
	}

	/*
	if(e.state.Z.abs > 0){
		forward();
	}

	if(e.state.Z.abs < 0){
		backward();
	}*/

	return true;
}
bool CameraController::mousePressed( const OIS::MouseEvent &e, OIS::MouseButtonID id )
{
	if( id == OIS::MouseButtonID::MB_Middle ){
		mDoMousePan = true;
		mAlpha = 0;
	}

	if( id == OIS::MouseButtonID::MB_Right )
		mDoRotate = true;

	return true;
}
bool CameraController::mouseReleased( const OIS::MouseEvent &e, OIS::MouseButtonID id )
{
	if( id == OIS::MouseButtonID::MB_Middle ){
		mDoMousePan = false;
	}
	if( id == OIS::MouseButtonID::MB_Right ){
		mDoRotate = false;
	}

	return true;
}

void CameraController::setTargetDirection( const Ogre::Vector3& direction )
{
	mTargetDirection = Ogre::Vector3( direction );
	mTargetDirection.normalise();
}

const Ogre::Vector3& CameraController::getRelativeGroundDirection( const Ogre::Vector3& direction )
{
	return direction;// mGroundPlane.projectVector( direction );
}
void CameraController::panAccelerate( Ogre::Real acceleration )
{
	mSpeed += mSpeed + acceleration;
	if(mSpeed > SPEED_LIMIT)
		mSpeed = SPEED_LIMIT;
}

void CameraController::panDecay( void )
{
	mSpeed -= ACCELERATION;
	if(mSpeed < 0)
		mSpeed = 0;
}

void CameraController::setKeyboardPanDirection( void )
{
	if(!doKeyboardPan()) return;

	if(mDoPanUp){
		setTargetDirection( getRelativeGroundDirection( mTargetNode->getOrientation() * Ogre::Vector3(0, 0, -1) ) );
	}else if(mDoPanDown){
		setTargetDirection( getRelativeGroundDirection( mTargetNode->getOrientation() * Ogre::Vector3(0, 0, 1) ) );
	}else if(mDoPanLeft){
		setTargetDirection( getRelativeGroundDirection( mTargetNode->getOrientation() * Ogre::Vector3(-1, 0, 0) ) );
	}else if(mDoPanRight){
		setTargetDirection( getRelativeGroundDirection( mTargetNode->getOrientation() * Ogre::Vector3(1, 0, 0) ) );
	}

	//mAlpha = 0;
	//mDirection = mTargetDirection;
}