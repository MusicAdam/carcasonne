#include "GameBoard.h"
#include "Game.h"
#include "CarcasonneTile.h"

GameBoard::GameBoard( gameObject_key key )
	: GameObject( key )
{
	mHighlightedTile = 0;
}


GameBoard::~GameBoard( void )
{
	//TODO
}

void GameBoard::update( Ogre::Real time )
{
}

TileObject* GameBoard::getTile( tile_index x, tile_index y )
{
	Ogre::Node* node = mSceneNode->getChild( TileObject::IndexPairToSceneNodeName( x, y ) );
	return Ogre::any_cast<TileObject*>(node->getUserAny());
}

bool 	GameBoard::mouseMoved (const OIS::MouseEvent& arg)
{
	Ogre::Ray mouseRay = Ogre::Root::getSingleton().getSceneManager("SceneManager")->getCamera("PlayerCam")->getCameraToViewportRay(
							float(arg.state.X.abs) / float(arg.state.width),
							float(arg.state.Y.abs) / float(arg.state.height));

	if( mHighlightedTile )
	{
		std::pair< bool, Ogre::Real > intersection = mouseRay.intersects( mHighlightedTile->getSceneNode()->_getWorldAABB() );

		if(intersection.first)
		{
			return mHighlightedTile->mouseMoved( arg );
		}else{
			mHighlightedTile->onUnhighlight();
			mHighlightedTile = 0;
		}
	}
	
	if( mHighlightedTile == 0 )
	{
		TileObject* tile = getRayIntersection( mouseRay );

		if(tile){
			mHighlightedTile = tile;
			mHighlightedTile->onHighlight();
		}
	}
	
	return true;
}
bool 	GameBoard::mousePressed (const OIS::MouseEvent& arg, OIS::MouseButtonID id)
{
	return true;
}
bool 	GameBoard::mouseReleased (const OIS::MouseEvent& arg, OIS::MouseButtonID id)
{
	return true;
}

TileObject* GameBoard::getRayIntersection( Ogre::Ray ray )
{
	auto it = mSceneNode->getChildIterator();
	while(it.hasMoreElements()){
		Ogre::SceneNode* node = dynamic_cast<Ogre::SceneNode*>(it.getNext());
		auto intersection = ray.intersects(node->_getWorldAABB());
		if(intersection.first){
			GameObject* game_obj = GameObject::GetAnyObject<GameObject>(node);
			return dynamic_cast<TileObject*>(game_obj);
		}
	}
	return 0;
}

/*
* Adds a tile to the board
* tile is added by inserting TileSceneNode into our scenenode
* triggering scenenode events
*/
void GameBoard::addTile( TileObject* tile )
{
	mSceneNode->addChild( tile->getSceneNode() );
	updateBounds();
}

/*
* Removes a tile by removing it from our SceneNode
*/
void GameBoard::removeTile( TileObject* tile )
{
	mSceneNode->removeChild( tile->getSceneNode() );
	updateBounds();
}

void GameBoard::removeTile( gameObject_key tileKey )
{
	removeTile( World::getInstance().getGameObject<TileObject>( tileKey ) );
}

Ogre::Real GameBoard::getWidth( void ) const
{
	return mSceneNode->_getWorldAABB().getSize().x;
}

Ogre::Real GameBoard::getHeight( void ) const
{
	return mSceneNode->_getWorldAABB().getSize().y;
}

void GameBoard::validate( void )
{
	if( mValid ) return;

	mSceneNode->_update(true, false);

	mValid = true;
}