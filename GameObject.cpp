#include "GameObject.h"
#include <iostream>


const gameObject_key GameObject::SCENE_NODE_NAME_TAG = "NODE";
const gameObject_key GameObject::ENTITY_NAME_TAG = "ENTITY";
const gameObject_key GameObject::NAME_SEPARATOR = "-";

GameObject::GameObject( gameObject_key key )
	: mSceneNode(0),
	  mKey(key)
{
}

GameObject::~GameObject(void)
{
}

void GameObject::update( Ogre::Real time )
{
}

Ogre::SceneNode* GameObject::spawn( Ogre::SceneNode* parentNode )
{
	std::string sceneName	= getKey() + NAME_SEPARATOR + SCENE_NODE_NAME_TAG;

	mSceneNode	= Ogre::Root::getSingleton().getSceneManager("SceneManager")->createSceneNode(sceneName);
	mSceneNode->setListener( this );
	//mSceneNode->showBoundingBox(true);
	mSceneNode->setUserAny( Ogre::Any( this ) );


	if( parentNode ){
		parentNode->addChild(mSceneNode);
	}

	return mSceneNode;
}

Ogre::SceneNode* GameObject::getSceneNode( void )
{
	return mSceneNode;
}

void GameObject::nodeAttached( const Ogre::Node* node )
{
	mValid = false;
}
void GameObject::nodeDestroyed( const Ogre::Node* node )
{
	mValid = false;
}
void GameObject::nodeDetached( const Ogre::Node* node )
{
	mValid = false;
}
void GameObject::nodeUpdated( const Ogre::Node* node )
{	
	mValid = false;
}

void GameObject::updateBounds( void )
{
	if( mValid && mMaintainBoundsHierarchy ) return;

	mBounds = Ogre::AxisAlignedBox();

	auto it = mSceneNode->getChildIterator();
	if(it.current() == it.end()){
		mSceneNode->_updateBounds();
		mSceneNode->_update(true, true);
		mBounds.merge(mSceneNode->_getWorldAABB());
	}else{
		while( it.hasMoreElements() )
		{
			GameObject* obj = GetAnyObject<GameObject>(it.getNext());
			obj->updateBounds();
			mBounds.merge( obj->getBounds() );
		}
	}

	mValid = true;
}

void GameObject::maintainBoundsHierarchy( bool toggle )
{
	mMaintainBoundsHierarchy = toggle;
	mValid = false;
}