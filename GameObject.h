#ifndef _GameObject_h_
#define _GameObject_h_

#include <OgreRoot.h>
#include <OgrePrerequisites.h>
#include <OgreEntity.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>
#include <OgreMaterial.h>
#include <OgreMaterialManager.h>
#include <OgreResourceGroupManager.h>
#include <OgreTechnique.h>

#include "Updatable.h"

typedef std::string gameObject_key;

class IGameObjectConstructor;

/*
 * Data to be passed to SceneNode's UserAny
 */
class GameObject;
struct game_object_data{
	GameObject* gameObject;

	
	template<typename T>
	T* getGameObject( void )
	{
		return dynamic_cast<T*>(gameObject);
	}
};

/*	
 *	GameObject defines a wrapper for an Ogre::Entity and the Ogre::SceneNode it's attached to.  
 */
class GameObject : public Updatable, public Ogre::Node::Listener
{
public:
	friend IGameObjectConstructor;
	
	static const gameObject_key ENTITY_NAME_TAG;
	static const gameObject_key SCENE_NODE_NAME_TAG;
	static const gameObject_key NAME_SEPARATOR;

	GameObject( gameObject_key key );
	virtual ~GameObject(void);

	template< typename T >
	static T* GetAnyObject( Ogre::Node* node )
	{
		return Ogre::any_cast<T*>(node->getUserAny());
	}

	/*
	 * Gets entity
	 */
	Ogre::Entity* getEntity( void );
    /*
	 * Get sceneNode the current entity is attached to
	 * returns NULL if mEntity == NULL or if it is not attached to a scene node
	 */
	Ogre::SceneNode* getSceneNode( void );
	/*
	 * Get Name
	 */
	Ogre::String getName( void );
	/*
	 * Gets the object's id
	 */
	virtual gameObject_key getKey(){ return mKey; }

	/*
	 * Update
	 */
	void update( Ogre::Real time );

	/*
	 * Ogre node events
	 */
	virtual void nodeAttached( const Ogre::Node* node );
	virtual void nodeDestroyed( const Ogre::Node* node );
	virtual void nodeDetached( const Ogre::Node* node );
	virtual void nodeUpdated( const Ogre::Node* node );

	/*
	 * Updates the SceneNode hierarchy's bounding box
	 */
	virtual void updateBounds( void );

	virtual Ogre::AxisAlignedBox getBounds( void )
	{ 
		updateBounds();
		if(mMaintainBoundsHierarchy)
			return mBounds; 
		if(mSceneNode)
			return mSceneNode->_getWorldAABB();
		return Ogre::AxisAlignedBox();
	}

	/*
	 * Creates its entity and attaches it to a node
	 * Returns entity
	 */
	virtual Ogre::SceneNode* spawn( Ogre::SceneNode* parentNode = 0 );

	virtual void maintainBoundsHierarchy( bool toggle );
protected:
	gameObject_key			mKey;
	Ogre::SceneNode*		mSceneNode;
	Ogre::AxisAlignedBox	mBounds;
	bool					mValid; //tracks bounds validity
	bool					mMaintainBoundsHierarchy; //keep mBounds valid
};

/*
 * GameObjectConstructor interface incase custom create function needs to be defined
 */
class IGameObjectConstructor
{
public:
	virtual GameObject* create( gameObject_key key ) const = 0;
};

#endif
