/*
-----------------------------------------------------------------------------
Filename:    Game.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "Game.h"
#include "World.h"
#include "CameraController.h"
#include "DebugDrawer.h"
#include "CarcasonneTile.h"
//---------------------------------------------------------------------------
Game::Game(void)
	:	mRayScnQuery(0)
{
}
//---------------------------------------------------------------------------
Game::~Game(void)
{
}

//---------------------------------------------------------------------------
void Game::createScene(void)
{
	new DebugDrawer(mSceneMgr, 0.5f);

	Ogre::Light* light = mSceneMgr->createLight("MainLight");
	light->setPosition(20, 80, 50);

	mSceneMgr->setAmbientLight(Ogre::ColourValue(.5f, .5f, .5f));
	mSceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

	mGameBoard = new GameBoard( "gameBoard" );
	mGameBoard->maintainBoundsHierarchy( true );
	mGameBoard->spawn( mSceneMgr->getRootSceneNode() );
	mInputManager->addMouseListener(mGameBoard, "mGameBoard_mouseListener");

	registerUpdatable( dynamic_cast<Updatable*>(mGameBoard) );
	
	auto tile = World::getInstance().createGameObject<CarcasonneTile>( "CarcasonneTile", TileObject::IndexPairToKey(0, 0) );
	tile->spawn( mGameBoard->getSceneNode() );
	tile->rotateRight();
	tile->debug_output();
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#	define WIN32_LEAN_AND_MEAN
#	include "windows.h"

#	ifdef _DEBUG
#		include <io.h>
#		include <windows.h>
#		include <stdio.h>
#		include <fcntl.h>
#		include <io.h>
#		include <iostream>
#		include <fstream>

void RedirectIOToConsole( void )
{
   // Create a console (if one doesn't already exist)
   AllocConsole();

   // Adjust the console parameters (scroll buffer)
   CONSOLE_SCREEN_BUFFER_INFO coninfo;
   GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &coninfo);
   coninfo.dwSize.Y = 1000;
   SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coninfo.dwSize);

   // Redirect stdout
   HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
   int handleOut = _open_osfhandle((intptr_t)hStdOut, _O_TEXT);
   FILE *fpOut = _fdopen(handleOut, "w");
   *stdout = *fpOut;
   setvbuf(stdout, NULL, _IONBF, 0);

   // Redirect stdin
   HANDLE hStdIn = GetStdHandle(STD_INPUT_HANDLE);
   int handleIn = _open_osfhandle((intptr_t)hStdIn, _O_TEXT);
   FILE *fpIn = _fdopen(handleIn, "r");
   *stdin = *fpIn;
   setvbuf(stdin, NULL, _IONBF, 0);

   // Redirect stderr
   HANDLE hStdErr = GetStdHandle(STD_ERROR_HANDLE);
   int handleErr = _open_osfhandle((intptr_t)hStdErr, _O_TEXT);
   FILE *fpErr = _fdopen(handleErr, "w");
   *stderr = *fpErr;
   setvbuf(stderr, NULL, _IONBF, 0);  

   // Sync other IO streams with stdio (cout, wcout, cin, wcin, wcerr, cerr, wclog, clog)
   std::ios::sync_with_stdio();
}

#	endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)	{ 
#	ifdef _DEBUG
		RedirectIOToConsole();
#	endif
#else
    int main(int argc, char *argv[])
    {
#endif
        // Create application object
        Game app;

        try {
            app.start();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
