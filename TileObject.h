#ifndef _TileObject_h_
#define _TileObject_h_

#include <string>

#include <OgreString.h>
#include <OgreRoot.h>
#include <OgreEntity.h>
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>
#include <OISMouse.h>

#include "GameObject.h"

typedef short tile_index;
typedef std::pair< tile_index, tile_index > index_pair;

class TileObject : public GameObject
{
public:
	static const float UNIT_SIZE;
	static const gameObject_key INDEX_SEPARATOR;

	/*
	 * Utility functions for keys 
	 */
	static index_pair KeyToIndexPair( const gameObject_key& key );
	static gameObject_key IndexPairToKey( const index_pair& indexPair );
	static gameObject_key IndexPairToKey( tile_index x, tile_index y );
	static gameObject_key OgreNameToKey( std::string name );
	static std::string IndexPairToSceneNodeName( tile_index x, tile_index y );


	TileObject( gameObject_key key );
	virtual ~TileObject(void);

	virtual void update( Ogre::Real time );
	virtual Ogre::SceneNode* spawn( Ogre::SceneNode* parentNode = 0 );

	tile_index getIndexX(void){ return mIndexX; }
	tile_index getIndexY(void){ return mIndexY; }

	virtual void attached(){} //TODO: Check for scene listener for this or move to a board listener
	virtual void detached(){}

	virtual void	onHighlight(); //TODO: Move to a selectable class
	virtual void	onUnhighlight(); //TODO: Move to a selectable class
	virtual bool	isHighlighted(); //TODO: Move to a selectable class
	
	virtual bool 	mouseMoved (const OIS::MouseEvent& arg);
	virtual bool 	mousePressed (const OIS::MouseEvent& arg, OIS::MouseButtonID id);
	virtual bool 	mouseReleased (const OIS::MouseEvent& arg, OIS::MouseButtonID id);
protected:
	bool					mHighlighted; //TODO: Move to a selectable class
private:
	tile_index					mIndexX;
	tile_index					mIndexY;
};

#endif