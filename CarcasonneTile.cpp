#include "CarcasonneTile.h"
#include "World.h"

REGISTER_GAME_OBJECT( CarcasonneTile );

CarcasonneTile::CarcasonneTile(  gameObject_key key )
	: TileObject( key )
{
	tile_definition.road_flags |= MASK_SIDE_LEFT | MASK_SIDE_RIGHT;
	debug_output();
}


CarcasonneTile::~CarcasonneTile(void)
{
}



void CarcasonneTile::rotateLeft( void )
{
	tile_definition.rotateLeft();
	if(getSceneNode()){
		getSceneNode()->rotate(Ogre::Vector3(0, 1, 0), Ogre::Radian(0.785398));
	}
}

void CarcasonneTile::rotateRight( void )
{
	tile_definition.rotateRight();
	if(getSceneNode()){
		getSceneNode()->rotate(Ogre::Vector3(0, 1, 0), -Ogre::Radian(0.785398));
	}
}

void CarcasonneTile::debug_output()
{
	std::cout << "Road Flags: " << std::endl;
	std::cout << "\tLeft: " << std::to_string((tile_definition.road_flags & MASK_SIDE_LEFT) > 0) << std::endl;
	std::cout << "\tRight: " << std::to_string((tile_definition.road_flags & MASK_SIDE_RIGHT) > 0) << std::endl;
	std::cout << "\tTop: " << std::to_string((tile_definition.road_flags & MASK_SIDE_TOP) > 0) << std::endl;
	std::cout << "\tBottom: " << std::to_string((tile_definition.road_flags & MASK_SIDE_BOTTOM) > 0) << std::endl;
}

void CarcasonneTile::onHighlight()
{
	if(!mHighlighted){
		rotateLeft();
	}

	TileObject::onHighlight();
}
